formgram.machines.finite\_automata.transformations module
=========================================================

.. automodule:: formgram.machines.finite_automata.transformations
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
