formgram.machines.finite\_automata.classifiers module
=====================================================

.. automodule:: formgram.machines.finite_automata.classifiers
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
