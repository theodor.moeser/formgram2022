formgram.classes.turing\_machine module
=======================================

.. automodule:: formgram.classes.turing_machine
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
