formgram.machines.turing\_machines.transformations module
=========================================================

.. automodule:: formgram.machines.turing_machines.transformations
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
