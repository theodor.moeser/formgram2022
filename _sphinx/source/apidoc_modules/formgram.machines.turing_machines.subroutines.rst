formgram.machines.turing\_machines.subroutines module
=====================================================

.. automodule:: formgram.machines.turing_machines.subroutines
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
