# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('../../src'))


# -- Project information -----------------------------------------------------

project = 'grammars'
copyright = '2022, Theodor Simon Möser'
author = 'Theodor Simon Möser'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
#    'rst2pdf.pdfbuilder',
    'sphinx.ext.autodoc',
    'sphinx_copybutton',
    'sphinx_autodoc_typehints',
    'sphinx.ext.viewcode',
    'sphinx_thebe',
    'sphinxcontrib.bibtex',
]

bibtex_bibfiles = ["references.bib"]
#pdf_stylesheets = ['twocolumn']

copybutton_prompt_text = ">>> "

#autoapi_type = 'python'
#autodoc_typehints = 'description'
#autoapi_dirs = ['../../src/grammars', '../../src/classes', '../../src/machines']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['venv', 'venv_linux', 'src/grammars/str_interface/tables']

# -- Options for HTML output -------------------------------------------------

thebe_config = {
   "always_load": True,
    "bootstrap": True,
    #"selector": ".thebe",
    "codemirror-config": {
        "theme": "idea",
        "electricChars": "true",
        "lineNumbers": "true",
        "indentWithTabs": "true",
        "indentUnit": 4,
    }
}


# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_book_theme'
html_theme_options = {
    "launch_buttons": {
        "thebe": True,
    },
    "repository_url": "https://github.com/binder-examples/requirements",
}
# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# -- Options for Latex builder -----------------------------------------------
