.. formgram documentation master file, created by
   sphinx-quickstart on Thu Jan 13 14:54:39 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to formgram's documentation!
====================================

.. include:: ../../README.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Readme_inclusion
   apidoc_modules/formgram.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
