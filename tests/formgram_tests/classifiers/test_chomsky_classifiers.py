"""
TODO DO EVERYTHING
"""
import os
from unittest import TestCase

from formgram_tests.helper_functions import get_grammars
import formgram.grammars.classifiers.chomsky_classifiers as cc


class TestFunctionsOnTextFiles(TestCase):
    def test_get_chomsky_type(self):
        """Test if the correct chomsky type is determined"""
        for chomsky_type in range(4):
            for grammar_file, grammar, _ in get_grammars(chomsky_type=chomsky_type):
                with self.subTest(msg=f"type-{chomsky_type}/{os.path.split(grammar_file)[-1]}"):
                    try:
                        retval = cc.get_chomsky_type(grammar)
                    except Exception as e:
                        self.fail(msg=e)
                    self.assertEqual(chomsky_type, retval)

    def test_is_grammar(self):
        """Test if all provided grammars are correctly recognized"""
        for grammar_file, grammar, _ in get_grammars():
            with self.subTest(msg=f"type-{type}/{os.path.split(grammar_file)[-1]}"):
                try:
                    retval = cc.is_grammar(grammar)
                except Exception as e:
                    self.fail(msg=e)
                self.assertTrue(retval)

    def test_is_grammar_negative(self):
        """Test if garbage is correctly refused"""
        self.assertFalse(cc.is_grammar("this is not a grammar"))

    def test_is_monotone(self):
        """Test if Type<1 grammars are rejected and Type1 grammars accepted"""
        for chomsky_type in range(4):
            for grammar_file, grammar, tags in get_grammars(chomsky_type=chomsky_type):
                with self.subTest(msg=f"type-{chomsky_type}/{os.path.split(grammar_file)[-1]}"):
                    try:
                        result = cc.is_monotone(grammar)
                    except Exception as e:
                        self.fail(msg=e)
                    if chomsky_type == 1:
                        self.assertTrue(result)
                    elif chomsky_type > 1:
                        if "not monotone" in tags:
                            self.assertFalse(result)
                        else:
                            self.assertTrue(result)
                    # remember context free grammars do not need to be monotone
                    else:
                        self.assertFalse(result)

    def test_is_context_sensitive(self):
        """Test if Type<1 grammars are rejected and only context sensitive ones with higher type are accepted"""
        for chomsky_type in range(4):
            for grammar_file, grammar, tags in get_grammars(chomsky_type=chomsky_type):
                filename = os.path.split(grammar_file)[-1]
                with self.subTest(msg=f"type-{chomsky_type}/{filename}"):
                    try:
                        result = cc.is_context_sensitive(grammar)
                    except Exception as e:
                        self.fail(msg=e)
                    if chomsky_type == 1 and "context sensitive" in tags:
                        self.assertTrue(result)
                    elif chomsky_type > 1 and "not monotone" not in tags:
                        self.assertTrue(result)
                    else:
                        self.assertFalse(result)

    def test_is_context_free(self):
        """Test if TYPE<2 grammars are rejected and TYPE>=2 grammars accepted"""
        for chomsky_type in range(4):
            for grammar_file, grammar, tags in get_grammars(chomsky_type=chomsky_type):
                with self.subTest(msg=f"type-{chomsky_type}/{os.path.split(grammar_file)[-1]}"):
                    try:
                        result = cc.is_context_free(grammar)
                    except Exception as e:
                        self.fail(msg=e)
                    if chomsky_type >= 2:
                        self.assertTrue(result)
                    else:
                        self.assertFalse(result)

    def test_single_side_linear(self):
        """Test if TYPE<3 grammars are rejected and TYPE3 grammars accepted"""
        for chomsky_type in range(4):
            for grammar_file, grammar, tags in get_grammars(chomsky_type=chomsky_type):
                with self.subTest(msg=f"type-{chomsky_type}/{os.path.split(grammar_file)[-1]}"):
                    try:
                        result = cc.is_single_side_linear(grammar)
                    except Exception as e:
                        self.fail(msg=e)
                    if chomsky_type == 3:
                        self.assertTrue(result)
                    else:
                        self.assertFalse(result)

    def test_is_regular(self):
        """Test if Type<3 grammars are rejected and regular Type3 grammars accepted (others rejected)"""
        for chomsky_type in range(4):
            for grammar_file, grammar, tags in get_grammars(chomsky_type=chomsky_type):
                with self.subTest(msg=f"type-{chomsky_type}/{os.path.split(grammar_file)[-1]}"):
                    try:
                        result = cc.is_regular(grammar)
                    except Exception as e:
                        self.fail(msg=e)
                    if chomsky_type == 3 and "regular" in tags:
                        self.assertTrue(result)
                    else:
                        self.assertFalse(result)
