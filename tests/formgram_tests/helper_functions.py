import os

from formgram.grammars import str_interface as parser


def get_grammars(chomsky_type=None):
    if chomsky_type not in range(4):
        for i in range(4):
            for return_value in get_grammars(i):
                yield return_value
        return
    dirname = os.path.dirname(__file__);
    chomsky_type_folder = os.path.join(dirname, "grammar_text_files", f"type_{chomsky_type}")
    grammar_files = [os.path.join(chomsky_type_folder, file) for file in os.listdir(chomsky_type_folder)]
    grammar_files = filter(lambda file: os.path.isfile(file), grammar_files)
    for grammar_path in grammar_files:
        with open(grammar_path) as grammar_file:
            content = grammar_file.read()
            grammar = parser.parse(content)
            grammar_tags = get_tags(content)
            yield os.path.relpath(grammar_file.name, chomsky_type_folder), grammar, grammar_tags


def get_tags(grammar_string: str) -> set:
    lines = grammar_string.split("\n")
    trimmed_lines = [line.strip() for line in lines]
    comment_lines = filter(lambda line: len(line) > 0 and line[0] == "#", trimmed_lines)
    comment_lines_content = set(map(lambda line: line[1:].strip(), comment_lines))
    tags = set()
    for line in comment_lines_content:
        line_tags = line.split(",")
        for tag in line_tags:
            tags.add(tag.strip())
    return tags
