import unittest

from formgram.grammars.str_interface import parse
from formgram.grammars.utility.unrestricted.helper import to_backus_naur_form
from formgram_tests.helper_functions import get_grammars


class TestBackusNaurForm(unittest.TestCase):
    def test_backus_naur_equivalence(self):
        for grammar_file, grammar, tags in get_grammars():
            with self.subTest(file=grammar_file):
                try:
                    bnf = to_backus_naur_form(grammar)
                except Exception as e:
                    self.fail(msg=e)
                self.assertEqual(parse(bnf), grammar)


if __name__ == '__main__':
    unittest.main()
