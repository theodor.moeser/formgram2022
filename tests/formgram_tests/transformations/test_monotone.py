import unittest

from formgram.grammars.classifiers.chomsky_classifiers import is_context_sensitive
from formgram.grammars.helper_functions.input_validator import validate_grammar_form
from formgram.grammars.transformations.monotone import to_context_sensitive_form
from formgram.grammars.utility.unrestricted.helper import to_backus_naur_form
from formgram_tests.helper_functions import get_grammars


class TestToContextSensitive(unittest.TestCase):
    def test_to_context_sensitive_transformation(self):
        for grammar_file, grammar, tags in get_grammars(chomsky_type=1):
            with self.subTest(file=grammar_file):
                try:
                    context_sensitive_grammar = to_context_sensitive_form(grammar)
                    validate_grammar_form(context_sensitive_grammar)
                except Exception as e:
                    self.fail(msg=e)
                if "context sensitive" in tags:
                    self.assertEqual(context_sensitive_grammar, grammar)
                else:
                    new_grammar_is_context_sensitive = is_context_sensitive(context_sensitive_grammar)
                    if not new_grammar_is_context_sensitive:
                        print(to_backus_naur_form(context_sensitive_grammar))
                    self.assertTrue(new_grammar_is_context_sensitive)


if __name__ == '__main__':
    unittest.main()
