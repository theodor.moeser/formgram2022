import unittest

from formgram.grammars.classifiers import form_classifiers
from formgram.grammars.helper_functions.input_validator import validate_grammar_form
from formgram.grammars.transformations.context_free import to_separated_terminals_form, \
    to_chomsky_normal_form, to_greibach_normal_form
from formgram.grammars.utility.unrestricted.helper import to_backus_naur_form
from formgram_tests.helper_functions import get_grammars


class TestCNFSeparation(unittest.TestCase):
    def test_copy_on_nothing_to_do(self):
        """Test if to seperated terminals form correctly does nothing if already in correct form"""
        for grammar_file, grammar, _ in get_grammars(chomsky_type=2):
            if form_classifiers.has_separated_terminals(grammar):
                with self.subTest(file=grammar_file):
                    separated_grammar = to_separated_terminals_form(grammar)
                    self.assertEqual(grammar, separated_grammar)
                    separated_grammar["terminals"].add("3")
                    self.assertNotEqual(grammar, separated_grammar)
                    try:
                        validate_grammar_form(separated_grammar)
                    except Exception as e:
                        self.fail(msg=e)


    def test_is_separated_after_application(self):
        """Test if has_seperated_terminals returns True after to_seperated_terminals_has been done"""
        for grammar_file, grammar, _ in get_grammars(chomsky_type=2):
            with self.subTest(file=grammar_file):
                separated_grammar = to_separated_terminals_form(grammar)
                self.assertTrue(form_classifiers.has_separated_terminals(separated_grammar), separated_grammar)
                try:
                    validate_grammar_form(separated_grammar)
                except Exception as e:
                    self.fail(msg=e)


class TestCNFTransformation(unittest.TestCase):
    def test_CNF_transformation(self):
        for grammar_file, grammar, tags in get_grammars(chomsky_type=2):
            with self.subTest(file=grammar_file):
                try:
                    cnf_grammar = to_chomsky_normal_form(grammar)
                    validate_grammar_form(cnf_grammar)
                except Exception as e:
                    self.fail(msg=e)
                if "chomsky normal form" in tags:
                    self.assertEqual(cnf_grammar, grammar)
                else:
                    is_cnf = form_classifiers.has_chomsky_normal_form(cnf_grammar)
                    if not is_cnf:
                        print(to_backus_naur_form(cnf_grammar))
                    self.assertTrue(is_cnf)


class TestGNFTransformation(unittest.TestCase):
    def test_GNF_transformation(self):
        for grammar_file, grammar, tags in get_grammars(chomsky_type=2):
            with self.subTest(file=grammar_file):
                try:
                    gnf_grammar = to_greibach_normal_form(grammar)
                    validate_grammar_form(gnf_grammar)
                except ValueError as e:#Exception as e:
                    self.fail(msg=e)
                if "greibach normal form" in tags:
                    self.assertEqual(gnf_grammar, grammar)
                else:
                    is_gnf = form_classifiers.has_greibach_normal_form(gnf_grammar)
                    if not is_gnf:
                        print(to_backus_naur_form(gnf_grammar))
                        print("~~~")
                    self.assertTrue(is_gnf)


if __name__ == '__main__':
    unittest.main()
