import unittest

from formgram.grammars.classifiers.chomsky_classifiers import is_left_linear, is_left_regular, \
    is_right_linear, is_right_regular
from formgram.grammars.helper_functions.input_validator import validate_grammar_form
from formgram.grammars.transformations.regular import to_left_linear_form, to_left_regular_form, \
    to_right_linear_form, to_right_regular_form
from formgram.grammars.utility.unrestricted.helper import to_backus_naur_form
from formgram_tests.helper_functions import get_grammars


class TestDirectionalTransformations(unittest.TestCase):
    def test_to_left_linear_form(self):
        for grammar_file, grammar, tags in get_grammars(chomsky_type=3):
            with self.subTest(file=grammar_file):
                try:
                    left_linear_grammar = to_left_linear_form(grammar)
                    validate_grammar_form(left_linear_grammar)
                except Exception as e:
                    self.fail(msg=e)
                if any(tag in tags for tag in ["left linear", "left regular"]):
                    self.assertEqual(left_linear_grammar, grammar)
                else:
                    try:
                        new_grammar_is_left_linear = is_left_linear(left_linear_grammar)
                    except Exception as e:
                        print(left_linear_grammar["productions"])
                        self.fail(msg=e)
                    if not new_grammar_is_left_linear:
                        print(to_backus_naur_form(left_linear_grammar))
                    self.assertTrue(new_grammar_is_left_linear)

    def test_to_left_regular_form(self):
        for grammar_file, grammar, tags in get_grammars(chomsky_type=3):
            with self.subTest(file=grammar_file):
                try:
                    left_regular_grammar = to_left_regular_form(grammar)
                    validate_grammar_form(left_regular_grammar)
                except Exception as e:
                    self.fail(msg=e)
                if any(tag in tags for tag in ["left regular"]):
                    self.assertEqual(left_regular_grammar, grammar)
                else:
                    try:
                        new_grammar_is_left_regular = is_left_regular(left_regular_grammar)
                    except Exception as e:
                        print(left_regular_grammar["productions"])
                        self.fail(msg=e)
                    if not new_grammar_is_left_regular:
                        print(to_backus_naur_form(left_regular_grammar))
                    self.assertTrue(new_grammar_is_left_regular)

    def test_to_right_linear_form(self):
        for grammar_file, grammar, tags in get_grammars(chomsky_type=3):
            with self.subTest(file=grammar_file):
                try:
                    right_linear_grammar = to_right_linear_form(grammar)
                    validate_grammar_form(right_linear_grammar)
                except Exception as e:
                    self.fail(msg=e)
                if any(tag in tags for tag in ["right linear", "right regular"]):
                    self.assertEqual(right_linear_grammar, grammar)
                else:
                    new_grammar_is_right_linear = is_right_linear(right_linear_grammar)
                    if not new_grammar_is_right_linear:
                        print(to_backus_naur_form(right_linear_grammar))
                    self.assertTrue(new_grammar_is_right_linear)

    def test_to_right_regular_form(self):
        for grammar_file, grammar, tags in get_grammars(chomsky_type=3):
            with self.subTest(file=grammar_file):
                try:
                    right_regular_grammar = to_right_regular_form(grammar)
                    validate_grammar_form(right_regular_grammar)
                except Exception as e:
                    self.fail(msg=e)
                if any(tag in tags for tag in ["right regular"]):
                    self.assertEqual(right_regular_grammar, grammar)
                else:
                    try:
                        new_grammar_is_right_regular = is_right_regular(right_regular_grammar)
                    except Exception as e:
                        print(right_regular_grammar["productions"])
                        self.fail(msg=e)
                    if not new_grammar_is_right_regular:
                        print(to_backus_naur_form(right_regular_grammar))
                    self.assertTrue(new_grammar_is_right_regular)


if __name__ == '__main__':
    unittest.main()
