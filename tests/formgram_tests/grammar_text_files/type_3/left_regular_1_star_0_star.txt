# tags:
# left linear, regular, type 3, epsilon special rule
< START > ::=  < 1 > '1'| < 1 > '0'| < 0 > '0' | '1' | '0' |
< 1 > ::= '1' | < 1 > '1'
< 0 > ::= '0' | < 0 > '0' | < 1 > '0'