"""

List of tags:
* type 0
* type 1
* type 2
* type 3
* context sensitive
* chomsky normal form
* greibach normal form
* not monotone
* epsilon special rule
* left linear
* right linear
* regular
"""