import unittest

from formgram.grammars.str_interface import parse


class TestTrimFunctionality(unittest.TestCase):
    def test_parsing_with_leading_newline(self):
        grammar_string = """
         <START> ::= <A> | 's' <START>
         's''s' <A> ::= 's' <A> 's'
         <A> ::= <START> <START>
         's' <A> 's' ::= 'a'
         <A> ::=
        """
        grammar = parse(grammar_string)


if __name__ == '__main__':
    unittest.main()
