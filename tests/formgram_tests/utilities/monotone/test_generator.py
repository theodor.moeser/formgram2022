import time
import unittest

from formgram.grammars.classifiers.form_classifiers import has_no_epsilon_productions
from formgram.grammars.transformations.context_free import to_epsilon_free_form
from formgram.grammars.utility.monotone.generator import generate_words_limited_by_length
from formgram_tests.helper_functions import get_grammars


class TestCreationOfShortWords(unittest.TestCase):
    def test_non_breaking(self):
        for grammar_file, grammar, tags in get_grammars():
            if "type 0" in tags:
                continue
            if not has_no_epsilon_productions(grammar):
                grammar = to_epsilon_free_form(grammar)
            with self.subTest(file=grammar_file):
                t = time.time()
                d = sorted(generate_words_limited_by_length(grammar, 2))



if __name__ == '__main__':
    unittest.main()
