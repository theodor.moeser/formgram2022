import math
import unittest
from itertools import product

from formgram.machines.pushdown_automata.grammar_interface import from_context_free_grammar, \
    to_context_free_grammar, create_possible_right_hand_side_tails
from formgram.machines.pushdown_automata.simulation_functions import run_full_simulation
from formgram.grammars.utility.monotone.generator import generate_words_limited_by_length
from formgram.grammars.utility.unrestricted.helper import to_backus_naur_form
from formgram_tests.helper_functions import get_grammars


class MyTestCase(unittest.TestCase):
    def test_from_context_free_grammar(self):
        for grammar_file, grammar, tags in get_grammars(chomsky_type=2):
            n = len(grammar["productions"])
            max_wordlen = round(math.log(10 ** 6, n ** 2))
            words = generate_words_limited_by_length(grammar, max_wordlen)
            if not words:
                continue
            with self.subTest(file=grammar_file):
                try:
                    pda = from_context_free_grammar(grammar)
                except Exception as e:
                    self.fail(msg=e)
                alphabet_star = {word for l in range(max_wordlen+1) for word in product(grammar["terminals"], repeat=l)}
                for word in alphabet_star:
                    included = (word in words)
                    simulated = run_full_simulation(pda, word)
                    if included == simulated:
                        pass  # all is well
                    else:
                        self.fail(f"word {word} was {'not' if not included else ''} in the language but {'not' if not simulated else ''} simulated by the pda")

    def test_to_push_down_automaton(self):
        for grammar_file, grammar, tags in get_grammars(chomsky_type=2):
            n = len(grammar["productions"])
            max_wordlen = round(math.log(10 ** 6, n ** 2))
            words = generate_words_limited_by_length(grammar, max_wordlen)
            if not words:
                continue
            with self.subTest(file=grammar_file):
                try:
                    pda = from_context_free_grammar(grammar)
                    new_grammar = to_context_free_grammar(pda)
                    new_words = generate_words_limited_by_length(new_grammar, max_wordlen)
                    if not words == new_words:
                        self.fail(f"new grammar \n{to_backus_naur_form(new_grammar)}\n creates different words: too few: {words.difference(new_words)}, too much: {new_words.difference(words)}")
                except Exception as e:
                    self.fail(msg=e)

    def test_create_possible_right_hand_side_tails(self):
        for stack_size in range(1, 10):
            for state_size in range(1, 10):
                with self.subTest(state_size=state_size, stack_size=stack_size):
                    try:
                        create_possible_right_hand_side_tails(range(stack_size), range(state_size), 0, stack_size)
                    except Exception as e:
                        self.fail(msg=e)


if __name__ == '__main__':
    unittest.main()
