"""#. states:
    Is a set of strings for the internal states of the machine
#. alphabet:
    Is a set of strings which can be on the tape
#. control_symbols:
    Is a set of strings which can also be on the tape. Especially the `blank` symbol
#. initial_state:
    Is an element of `states` which is the state of a freshly started machine
#. accepting_states:
    Is a subset of `states` which decides if a halting machine accepts
#. blank_symbol:
    Is an element of control_symbols which symbolizes an unwritten cell on tape
#. transitions:
    Is a set of tuples which the machine uses to operate. Each of the entries
    is of the form ``(current_state, read_symbol), (next_state, write_symbol, head_move_direction)``

"""

import unittest
from itertools import product

from formgram.machines.turing_machines.grammar_interface import to_grammar
from formgram.machines.turing_machines.simulation_functions import run_full_simulation
from formgram.grammars.utility.unrestricted.generator import generate_words_in_limited_steps
from formgram.grammars.utility.unrestricted.helper import to_backus_naur_form


class TestGrammarCreationFunction(unittest.TestCase):
    def test_single_case(self):
        simple_tm = {  # accepts alternating nonempty strings of 1 and 0
            "states": {"QI", "Q0", "Q1", "QF"},
            "alphabet": {"0", "1"},
            "control_symbols": {"."},
            "initial_state": "QI",
            "accepting_states": {"QF"},
            "blank_symbol": ".",
            "transitions": {
                (("QI", "0"), ("Q0", "0", "R")),
                (("QI", "1"), ("Q1", "1", "R")),
                (("Q0", "1"), ("Q1", "1", "R")),
                (("Q1", "0"), ("Q0", "0", "R")),
                (("Q0", "."), ("QF", ".", "L")),
                (("Q1", "."), ("QF", ".", "L")),
            }
        }
        grammar = to_grammar(simple_tm)
        print(to_backus_naur_form(grammar))
        test_length = 4
        alphabet_star_len = {word for l in range(1, test_length) for word in product({"0", "1"}, repeat=l)}
        acceptable_words = set()
        other_words = set()

        for word in alphabet_star_len:
            if all(word[i] != word[i+1] for i in range(len(word)-1)):
                acceptable_words.add(word)
            else:
                other_words.add(word)

        for word in acceptable_words:
            self.assertTrue(run_full_simulation(simple_tm, word, fuel=10**2))
        for word in other_words:
            self.assertFalse(run_full_simulation(simple_tm, word, fuel=10**2))

        words_to_find = acceptable_words.copy()
        grammar_words = generate_words_in_limited_steps(grammar, steps=16)
        print("start iterating")
        for word in grammar_words:
            self.assertNotIn(word, other_words)
            if len(word) < test_length:
                self.assertIn(word, acceptable_words)
            if word in words_to_find:
                words_to_find.remove(word)
            if len(words_to_find) == 0:
                break
            print(f"found {word}")


if __name__ == '__main__':
    unittest.main()
